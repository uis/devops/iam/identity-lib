#!/usr/bin/env python3
from typing import Optional
import argparse
import logging
from pathlib import Path
import yaml
import tempfile
from dataclasses import dataclass
import git
import os
import shutil
from urllib.parse import quote

LOG = logging.getLogger(Path(__file__).stem)

SPECS_FOLDER = "specs"
COMMIT_MESSAGE = "Update OpenAPI specs"


@dataclass
class SpecConfig:
    repo: str
    branch: str
    commit: Optional[str] = "HEAD"
    source: Optional[str] = "openapi.yaml"
    target: Optional[str] = None


def load_config(source: str) -> dict[str, SpecConfig]:
    LOG.info(f"Reading configuration from {source}")
    with open(source) as fp:
        sources = yaml.safe_load(fp)
    spec_configs = {}
    for name, c in sources.items():
        spec = SpecConfig(**c)
        if spec.target is None:
            spec.target = os.path.join(SPECS_FOLDER, f"{name}.yaml")
        spec_configs[name] = spec
    return spec_configs


def pull_specs(config: dict[str, SpecConfig], token: Optional[str]) -> set[str]:
    spec_files = set()
    for name, c in config.items():
        LOG.info(f"{name}:")
        repo = c.repo
        if token:
            # Token given so insert in to url, default to using USER env for username as
            # this is needed for personal access tokens and ignored for project access tokens
            creds = quote(os.getenv("USER", "token")) + ":" + quote(token) + "@"
            repo = repo.replace("https://", f"https://{creds}")
        LOG.info(f"...cloning from {c.repo}{' (using token)' if token else ''} @ {c.branch}")
        with tempfile.TemporaryDirectory() as tmp_dirname:
            # Clone repo@branch in to temp directory without checkout then checkout
            # just the file we want before copying it into this repo and recording
            # the SHA of the last commit to affect it
            repo = git.Repo.clone_from(
                url=repo,
                to_path=tmp_dirname,
                branch=c.branch,
                no_checkout=True,
            )
            LOG.info(f"...checking out {c.source}")
            repo.git.checkout(c.commit, c.source)

            LOG.info(f"...copying {c.source} to {c.target}")
            shutil.copyfile(os.path.join(tmp_dirname, c.source), c.target)

            last_commit = next(repo.iter_commits(rev=c.commit, paths=c.source, max_count=1))
            commit_sha = "Unknown" if last_commit is None else last_commit.hexsha
            LOG.info(f"...sha of {c.source} is {commit_sha}")
            sha_file = os.path.splitext(c.target)[0] + ".sha"
            with open(sha_file, "w") as fp:
                fp.write(commit_sha)

            spec_files.add(c.target)
            spec_files.add(sha_file)

    return spec_files


def commit_specs(spec_files: set[str]):
    this_repo_path = str(Path(__file__).parent.parent)  # Assume script is in subfolder in repo
    LOG.info(f"Committing to repo {this_repo_path}")
    repo = git.Repo(this_repo_path)
    if str(repo.head.ref) in {"master", "main"}:
        LOG.error("...in master/main branch - aborting commit")
    elif repo.index.diff("HEAD"):
        LOG.error("...files already staged - aborting commit")
    else:
        LOG.info(f'...adding {", ".join(list(spec_files))}')
        repo.index.add(spec_files)
        changes = len(repo.index.diff("HEAD"))
        if changes:
            LOG.info(f"...{changes} files need committing")
            repo.index.commit(COMMIT_MESSAGE)
        else:
            LOG.info("...no changes need committing")


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("--configuration", "-c", type=str, default="sources.yaml")
    parser.add_argument("--token", "-t", type=str)
    parser.add_argument("--commit", action="store_true")
    parser.add_argument("--debug", action="store_true")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)
    config = load_config(args.configuration)
    spec_files = pull_specs(config, args.token)
    if args.commit:
        commit_specs(spec_files)
