import importlib
import pkgutil
from os.path import exists
import pyclbr
from yaml import safe_load

from identitylib.card_client_configuration import CardClientConfiguration
from identitylib.card_client import ApiClient as CardApiClient
from identitylib.card_client import V1beta1Api as CardApi

from identitylib.photo_client_configuration import PhotoClientConfiguration
from identitylib.photo_client import ApiClient as PhotoApiClient
from identitylib.photo_client import V1beta1Api as PhotoApi

from identitylib.university_hr_configuration import UniversityHRClientConfiguration
from identitylib.hr_client import ApiClient as HRApiClient
from identitylib.hr_client import StaffMembersApi

from identitylib.university_student_configuration import UniversityStudentClientConfiguration
from identitylib.student_client import ApiClient as StudentApiClient
from identitylib.student_client import StudentsApi

from identitylib.lookup_client_configuration import LookupClientConfiguration
from identitylib.lookup_client import ApiClient as LookupApiClient
from identitylib.lookup_client import GroupApi, InstitutionApi, IbisApi, PersonApi


class APILoader:
    def __init__(self, config_file):
        with open(config_file) as cnf:
            self.config = safe_load(cnf)

    def load_card(self, api_key=None, api_secret=None, base_url=None):
        "Returns config, client, api_inst for the card API"
        conf = CardClientConfiguration(
            api_key or self.config["card"]["key"],
            api_secret or self.config["card"]["secret"],
            base_url=base_url or self.config["card"]["base_url"],
        )
        client = CardApiClient(conf)
        inst = CardApi(client)
        return conf, client, inst

    def load_photo(self, api_key=None, api_secret=None, base_url=None):
        "Returns config, client, api_inst for the photo API"
        conf = PhotoClientConfiguration(
            api_key or self.config["photo"]["key"],
            api_secret or self.config["photo"]["secret"],
            base_url=base_url or self.config["photo"]["base_url"],
        )
        client = PhotoApiClient(conf)
        photo_inst = PhotoApi(client)
        return conf, client, photo_inst

    def load_hr(self, api_key=None, api_secret=None, base_url=None):
        "Returns config, client, api_inst for the hr API"
        conf = UniversityHRClientConfiguration(
            api_key or self.config["hr"]["key"],
            api_secret or self.config["hr"]["secret"],
            base_url=base_url or self.config["hr"]["base_url"],
        )
        client = HRApiClient(conf)
        inst = StaffMembersApi(client)
        return conf, client, inst

    def load_student(self, api_key=None, api_secret=None, base_url=None):
        "Returns config, client, api_inst for the student API"
        conf = UniversityStudentClientConfiguration(
            api_key or self.config["student"]["key"],
            api_secret or self.config["student"]["secret"],
            base_url=base_url or self.config["student"]["base_url"],
        )
        client = StudentApiClient(conf)
        inst = StudentsApi(client)
        return conf, client, inst

    def _load_lookup(self, api_key=None, api_secret=None, base_url=None):
        conf = LookupClientConfiguration(
            api_key or self.config["lookup"]["key"],
            api_secret or self.config["lookup"]["secret"],
            base_url=base_url or self.config["lookup"]["base_url"],
        )
        client = LookupApiClient(conf)
        return conf, client

    def load_lookup(self, api_type, api_key=None, api_secret=None, base_url=None):
        "Returns config, client, and the specified lookup API instance"
        conf, client = self._load_lookup(api_key, api_secret, base_url)

        api_class_map = {
            "group": GroupApi,
            "institution": InstitutionApi,
            "ibis": IbisApi,
            "person": PersonApi,
        }

        api_instance = api_class_map.get(api_type)(client)
        return conf, client, api_instance

    def load_lookup_group(self, api_key=None, api_secret=None, base_url=None):
        "Returns config, client, api_inst for lookup group API"
        return self.load_lookup("group", api_key, api_secret, base_url)

    def load_lookup_institution(self, api_key=None, api_secret=None, base_url=None):
        "Returns config, client, api_inst for the lookup institution API"
        return self.load_lookup("group", api_key, api_secret, base_url)

    def load_lookup_ibis(self, api_key=None, api_secret=None, base_url=None):
        "Returns config, client, api_inst for the lookup ibis API"
        return self.load_lookup("group", api_key, api_secret, base_url)

    def load_lookup_person(self, api_key=None, api_secret=None, base_url=None):
        "Returns config, client, api_inst for the lookup person API"
        return self.load_lookup("group", api_key, api_secret, base_url)


def get_module_structure(module, _pre_path="", _structure={}):
    """
    Recursively builds and returns a dictionary containing the structure of a python package.

    Do not provide the two optional arguments to this function, they are used internally only.
    """
    _structure[module.__name__] = {}
    for importer, modname, ispkg in pkgutil.walk_packages(module.__path__):
        import_path = f"{module.__name__}.{modname}"
        if ispkg:
            spec = pkgutil._get_spec(importer, modname)
            if _pre_path:
                n_pre_path = _pre_path + module.__name__ + "."
            else:
                n_pre_path = module.__name__ + "."
            inner_module = importlib.util.module_from_spec(spec)
            get_module_structure(
                inner_module, _pre_path=n_pre_path, _structure=_structure[module.__name__]
            )
        else:
            _structure[module.__name__][modname] = list(
                pyclbr.readmodule_ex(_pre_path + import_path)
            )
    return _structure


if exists("config.yaml"):
    LOADER = APILoader("config.yaml")
