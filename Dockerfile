# This Dockerfile is just used for testing purposes and therefore builds in tox
# to the image.

###############################################################################
# Base image, which is used for the main building image and the scripts
FROM python:3.9 as base

# Some performance and disk-usage optimisations for Python within a docker container.
ENV PYTHONUNBUFFERED=1 PYTHONDONTWRITEBYTECODE=1

WORKDIR /usr/src/app

RUN pip install --upgrade pip
RUN pip install --no-cache-dir poetry
COPY pyproject.toml poetry.lock ./

###############################################################################
# Script base, install script requirements
FROM base as script-base

RUN poetry export --format=requirements.txt --only=scripts --output=script-requirements.txt
RUN pip install -r script-requirements.txt

###############################################################################
# Interactive tests image
FROM script-base as interactive-test

ARG install_from="--index-url https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple/ --pre ucam-identitylib"

COPY scripts/api-loader.py .
COPY scripts/api-loader-config.yaml ./config.yaml
COPY dist ./dist

RUN pip install $install_from

ENTRYPOINT ["python", "-i", "api-loader.py"]

###############################################################################
# Pull specs image
FROM script-base as pull-specs

RUN apt install git

COPY scripts/pull-openapi-specs.py .
COPY sources.yaml .

ENTRYPOINT ["./pull-openapi-specs.py"]

###############################################################################
# Openapi generator
# Needs to be >= 7.1.0 to use pydantic v2
# Needs to be > 7.3.0 to resolve identity-lib#40
FROM openapitools/openapi-generator-cli:v7.6.0 as builder

WORKDIR /generated

COPY ./specs /specs

RUN docker-entrypoint.sh generate \
    --input-spec /specs/card.yaml \
    --generator-name python \
    --additional-properties packageName=identitylib.card_client \
    --output /generated

RUN docker-entrypoint.sh generate \
    --input-spec /specs/hr.yaml \
    --generator-name python \
    --additional-properties packageName=identitylib.hr_client \
    --output /generated

RUN docker-entrypoint.sh generate \
    --input-spec /specs/lookup.yaml \
    --generator-name python \
    --additional-properties packageName=identitylib.lookup_client \
    --output /generated

RUN docker-entrypoint.sh generate \
    --input-spec /specs/photo.yaml \
    --generator-name python \
    --additional-properties packageName=identitylib.photo_client \
    --output /generated

RUN docker-entrypoint.sh generate \
    --input-spec /specs/student.yaml \
    --generator-name python \
    --additional-properties packageName=identitylib.student_client \
    --output /generated

RUN docker-entrypoint.sh generate \
    --input-spec /specs/inst_identifier.yaml \
    --generator-name python \
    --additional-properties packageName=identitylib.inst_identifier_client \
    --output /generated

###############################################################################
# Complete built image
FROM base as full

WORKDIR /usr/src/lib

COPY --from=builder /generated/identitylib /usr/src/lib/identitylib

# Check to see if the python code is compilable
# (essentially a quick-and-dirty integration test to see if the OpenAPI code was correctly generated)
RUN echo '\n\
import ast\n\
import os\n\
from pathlib import Path\n\
ROOT = "/usr/src/lib/identitylib"\n\
for (dirpath, dirnames, filenames) in os.walk(ROOT):\n\
    for filename in filenames:\n\
        p = Path(dirpath) / filename\n\
        if p.suffix == ".py":\n\
            try:\n\
                ast.parse(open(p).read())\n\
            except Exception as err:\n\
                raise AssertionError(f"File fails to compile: {p}") from err' | python

RUN pip install --no-cache-dir tox==4.6.4

ADD . .

# Write tox requirements file
RUN poetry export --format=requirements.txt --with=dev --output=tox-requirements.txt

# Build the distribution and move to the root of the filesysteem
# which allows us to use it in downstream GitLab jobs.
RUN poetry build && mv dist /dist
