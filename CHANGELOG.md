# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.3.1] - 2024-05-30

### Fixed
- Photo API had `AllowAny` permissions incorrectly set for some of the
  endpoints.
- Importing some modules/subpackages for the Card API and Photo API
  previously failed to import.

## [3.3.0] - 2024-02-22

### Changed
- Update openapi generator to v7.3.0
- Pydantic dependency updated to ^2.6.1

## [3.2.0] - 2024-01-17

### Added
- Card API discontinued identifiers `delete` action.

### Changed
- Set the discontinued identifiers' `permitted_identifier` JSON field to
  optional rather than required.

## [3.1.2] - 2024-01-04

### Changed

- Downgrade Python version to ^3.9.
- Updates API specifications to latest versions.

## [3.1.1] - 2023-10-02

### Changed

- Updates API specifications to latest versions.

## [3.1.0] - 2023-09-21

### Changed

- Relaxed version specifier for urllib3 to match [upstream
  recommendation](https://urllib3.readthedocs.io/en/latest/v2-migration-guide.html#migrating-as-a-package-maintainer).
- Updates API specifications to latest versions.

## [3.0.0] - 2023-08-31

### Changed

- Changed to generator version 7.0.0, introduces various significant structural changes.
  For full release description for openapi generator v7 see https://github.com/OpenAPITools/openapi-generator/releases/tag/v7.0.0
- Now using card API spec at openapi 3.0.

## [2.0.2] - 2023-08-21

### Added

- Add smoke tests.
- Update open-api specs.

## [2.0.1] - 2023-08-10

### Added

- Add `ordering` parameter to Card API `/card-requests` endpoint.

## [2.0.0] - 2023-06-14

### Added

- Add `repair` operation to Photo API `/photo-identifiers` endpoint
- Add `repair` operation to Card API `/card-identifiers` endpoint
- Add `originating_card_request`, `originating_card_request__isnull`, `created_at`, \
  `issued_at`, `expires_at__isnull`, `issued_at__isnull` filters to `./cards` endpoint
- Add `created_at` to `/cards/{id}` endpoint
- Add new endpoint `/cards/update`
- Lowercase CRSID identifiers when calling Identifier.from_string()

### Changed

- Photo API `set_retention` operation replaced by `repair` operation.
- Card API `set_retention` operation replaced by `repair` operation.
- Photo API OpenAPI spec refactor causing breaking changes in identitylib.

## [1.6.0] - 2023-04-21

### Added

- Add UNACTIVATED state to card model.
- Add `refresh` action to /cards endpoint.

### Fixed

- Correctly defined CardIdentifier separate from CardIdentifierSummary.

## [1.5.0] - 2023-04-21

### Added

- Photo API 'soft_delete' action to /photo-identifiers endpoint.
- Photo API /photo-identifiers filter by is_highest_primary_identifier.
- Photo API /photo-identifiers bulk update endpoint.

## [1.4.0] - 2023-04-05

### Added

- Card API /card-identifiers bulk update endpoint.
- Card API added 'soft_delete' action to /card-identifiers endpoint.

## [1.3.0] - 2023-03-30

### Added

- Card API /card-identifiers filter by is_highest_primary_identifier.

### Changed

- Card API /card-identifiers delete operation returns 200.

## [1.2.0] - 2023-03-16

### Added

- New Card API /card-identifiers actions 'restore' and 'hard_delete'.
- New Photo API /card-identifiers actions 'restore' and 'hard_delete'.

### Changed

- Card API /card-identifiers action 'update' renamed to 'set_retention'.
- Photo API /card-identifiers action 'update' renamed to 'set_retention'.

## [1.1.0] - 2023-03-08

### Added

- New Card API /card-identifiers endpoint.
- New Photo API /photo-identifiers endpoint.
- Support image/heif image type in Photo API.
- Allow submit of photo reject reason in Photo API.
- Identifier RETENTION_PERIOD.

### Fixed

- Removed `format: uuid` from Photo API `openapi.spec` to maintain compatibility with Apigee.

## [1.0.18] - 2022-12-15

### Changed

- Revert Student API date of birth able to be null (they are now removed from the response instead)

## [1.0.17] - 2022-12-14 (Unpublished)

### Added

- Added CardRequest affiliations to be a list in Card API and force to be a list

### Changed

- Allow Student API date of birth to be null

## [1.0.16] - 2022-11-11

### Added

- Added ability to set the college-institution-id for each card request via the Card API.
- Added college-institution-ids list endpoint to the Card API.

### Changed

- Updated requirements to allow ./pull-specs.sh from from blank environment.

## [1.0.15] - 2022-10-24

### Added

- Filter for expires greater than or less than provided date to card list.
- Ability to get list of all notes and individual notes by id.

## [1.0.14] - 2022-10-13

### Added

- Clarified CARD_REQUEST_CREATOR permissions as applied to the Card API /update endpoint.
- Added /all-photos endpoint to the Photo API.

### Fixed

- Allow HR API to return nullable start and end dates.

## [1.0.13] - 2022-09-28

### Added

- `created_at__lte` and `created_at__gte` filters to the card API client.
- Invalidated state for Photos.
- Added Card API 'refresh' action.

### Changed

- Card API 'refute' action renamed to 'requeue' action.

## [1.0.12] - 2022-09-27

### Changed

- Fixed default base URLs for student and staff APIs
- Switched to 'python-prior' generator to maintain compatibility following v6.2.0 update

## [1.0.11] - 2022-09-06

### Changed

- Refresh OpenAPI spec.

## [1.0.10] - 2022-08-23

### Added

- Lookup API.

## [1.0.9] - 2022-08-18

### Changed

- Refresh OpenAPI spec.

## [1.0.8] - 2022-08-04

### Added

- Institution mapping API.

### Changed

- Refresh OpenAPI spec.

## [1.0.7] - 2022-07-14

### Changed

- Refresh OpenAPI spec.

## [1.0.6] - 2022-05-10

### Added

- University HR API and University Student API

## [1.0.5] - 2022-03-31

### Added

- Lookup institution scheme.

## [1.0.4] - 2022-03-25

### Added

- Depreciated student inst id scheme being used by institutional mapping API.

## [1.0.3] - 2022-03-22

### Changed

- Refresh OpenAPI spec.

## [1.0.2] - 2022-03-05

### Fixed

- Fix client credentials access token not being refreshed.

## [1.0.1] - 2022-01-09

### Added

- New Card API card-rfid-data-config endpoint.

## [1.0.0] - 2022-01-20

### Changed

- Refactor to use openapi-generator to generate the card_client and photo_client.

### Removed

- Removed previous card_client and photo_client modules (breaking change).

## [0.1.7] - 2021-11-10

### Fixed

- Fixed identity APIs making use of an expired token.

## [0.1.2] - 2021-08-27

### Added

- Basic Photo API client allowing fetching of transient photo urls.

## [0.1.1] - 2021-07-14

### Fixed

- Fixed installation when using Python 3.6 or lower, by adding dependency on dataclasses.

## [0.1.0] - 2021-07-14

### Added

- Initial version of this library, include identifier schemes and identifier parsing code.
